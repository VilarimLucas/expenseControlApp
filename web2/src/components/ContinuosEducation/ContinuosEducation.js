import React from 'react'
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';

const ContinuosEducation = () => {
  return (
    <Card className="continuos-education-card">
      <Card.Header>Educação Continuada</Card.Header>
      <ListGroup variant="flush">
        <ListGroup.Item>Como vender seu negócio</ListGroup.Item>
        <ListGroup.Item>E-commerce para principiantes</ListGroup.Item>
        <ListGroup.Item>Métodos de Persuasão</ListGroup.Item>
      </ListGroup>
    </Card>
  )
}

export default ContinuosEducation
