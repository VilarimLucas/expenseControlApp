import React from 'react'
import Card from 'react-bootstrap/Card';

const index = () => {
    return (

        <Card className="expense-card">
            <Card.Header>Seu custo de vida</Card.Header>
            <Card.Body>
                <Card.Title><small>R$</small>00<small>,00</small></Card.Title>
                <Card.Text>
                    Aqui vai o conteúdo do Card
                </Card.Text>
            </Card.Body>
            <Card.Footer className="text-muted">DESEMBRO 2023</Card.Footer>

        </Card>

    )
}

export default index
