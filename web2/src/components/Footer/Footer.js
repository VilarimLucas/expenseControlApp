import React from 'react';
import { MDBFooter } from 'mdb-react-ui-kit';

const Footer = () => {
    return (
        <MDBFooter className='text-center text-lg-start text-muted'>

            <div className='text-center p-4' >
                Copyright © 2023 Designed by Expense Control. Todos os Direitos Reservados
                <br />By: <a className='text-reset fw-bold' href='#'>
                    Expense Control Devs
                </a>
            </div>
        </MDBFooter>
    )
}

export default Footer
