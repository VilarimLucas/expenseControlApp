import React from 'react'

import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Form from 'react-bootstrap/Form';


function Header() {
    return (
        <Navbar className="px-0" style={{ backgroundColor: 'transparent' }}>
            <Container>
                <Navbar.Brand href="/home"> Expense Control</Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text>
                        <Form.Select aria-label="Default select example">
                            <option>Nome de Usuário</option>
                            <option value="1">sair</option>
                        </Form.Select>
                    </Navbar.Text>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default Header;