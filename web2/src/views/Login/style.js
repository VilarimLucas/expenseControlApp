import styled from "styled-components";
import { Navbar } from 'react-bootstrap';

export const SideLeft = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: left;
    padding: 20px;
    left:0;
    right:0;
    margin:0;
    height: 100vh;
    background-color: #3498DB;
    h1, hr{
        color: #fff;
    }
    .btn{
        padding: 15px;
        border-radius: 20px;
    }
`;

export const SideRight = styled.div`
    position: relative;
    height: 100vh;
`;

export const StyledNavbar = styled(Navbar)`
    position: absolute;
    width: 100%;
    top: 0;
    left: 0;
    z-index: 10;
    color: white; // Define a cor do texto para branco
    padding:1%;
    .navbar-brand, .navbar-nav .nav-link, .navbar-toggler-icon {
        color: white !important; // Garante que todos os textos e ícones sejam brancos
    }

    .navbar-toggler {
        border-color: white; // Cor da borda do botão do menu em telas menores
    }
`;

export const Footer = styled.footer`
    position: absolute;
    bottom: 0;
    right: 0;
    width: 80%;
    border-top: 1px solid white; // Linha branca na parte superior
    color: white; // Cor do texto
    text-align: right;
    padding: 10px; // Espaçamento vertical
    margin:10px;
    background-color: transparent; // Fundo transparente
`;