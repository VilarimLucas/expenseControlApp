import React, { useState } from 'react';
import Axios from 'axios';
import { useNavigate } from 'react-router-dom'; // ou useHistory para React Router v5
import Carousel from 'react-bootstrap/Carousel';
import * as C from './style';
import { Navbar } from 'react-bootstrap';

const Login = () => {
    const navigate = useNavigate(); // ou useHistory para React Router v5
    const [userEmail, setUserEmail] = useState('');
    const [userPassword, setUserPassword] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await Axios.post('http://localhost:4000/user/login', {
                email: userEmail,
                password: userPassword
            });
            if (response.status === 200) {
                navigate('/home');
            }
            // Você pode adicionar mais lógica aqui, por exemplo, lidar com diferentes status codes
        } catch (error) {
            console.error('Erro ao tentar fazer login', error);
            // Trate aqui os erros de login, como mostrar uma mensagem para o usuário
        }
    };

    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-3 px-0">
                    <C.SideLeft>
                        <h1>Login</h1>
                        <br />
                        <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <input 
                        type="email" 
                        className="form-control" 
                        placeholder="Email" 
                        value={userEmail}
                        onChange={(e) => setUserEmail(e.target.value)}
                        required 
                    />
                </div>
                <div className="mb-3">
                    <input 
                        type="password" 
                        className="form-control" 
                        placeholder="Senha" 
                        value={userPassword}
                        onChange={(e) => setUserPassword(e.target.value)}
                        required 
                    />
                </div>
                <div className="d-grid gap-2">
                    <button type="submit" className="btn btn-outline-light">Entrar</button>
                    <hr />
                    <a href="/cad-user" className="btn btn-outline-light">Não tenho cadastro</a>
                </div>
            </form>
                    </C.SideLeft>
                </div>
                <div className="col-md-9 px-0">
                    <C.SideRight>
                        <C.StyledNavbar expand="lg">
                            <Navbar.Brand> Expense Control App</Navbar.Brand>
                            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        </C.StyledNavbar>
                        <Carousel>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src="/assets/img/carousel/5.jpg"
                                    alt="Primeiro slide"
                                    style={{ height: '100vh', objectFit: 'cover' }}
                                />
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src="/assets/img/carousel/2.jpg"
                                    alt="Segundo slide"
                                    style={{ height: '100vh', objectFit: 'cover' }}
                                />
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src="/assets/img/carousel/4.jpg"
                                    alt="Terceiro slide"
                                    style={{ height: '100vh', objectFit: 'cover' }}
                                />
                            </Carousel.Item>
                        </Carousel>
                        <C.Footer>
                            Todos os direitos reservados
                        </C.Footer>
                    </C.SideRight>
                </div>
            </div>
        </div>
    );
}

export default Login;
