import React, { useState } from 'react';
import Axios from 'axios';
import { useNavigate } from 'react-router-dom'; // ou useHistory para React Router v5
import Carousel from 'react-bootstrap/Carousel';
import * as C from './style';
import { Navbar } from 'react-bootstrap';

const CadLogin = () => {
    const navigate = useNavigate(); // ou useHistory para React Router v5
    const [userName, setUserName] = useState('');
    const [userEmail, setUserEmail] = useState('');
    const [userPassword, setUserPassword] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await Axios.post('http://localhost:4000/user/add', {
                userName,
                userEmail,
                userPassword,
                costOfLiving: 0,
                totalIncomeUser: 0
            });
            console.log(response.data);
            navigate('/'); // Redireciona para a rota de login
        } catch (error) {
            console.error('Houve um erro ao enviar os dados', error);
        }
    };

    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-6 px-0">
                    <C.SideLeft>
                        <h1>Cadastrar Usuário</h1>
                        <br />
                        <form onSubmit={handleSubmit} >
                            <div className="mb-3">
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Digite o nome de Usuário"
                                    value={userName}
                                    onChange={(e) => setUserName(e.target.value)}
                                    required
                                />
                            </div>
                            <div className="mb-3">
                                <input
                                    type="email"
                                    className="form-control"
                                    placeholder="Digite um Email"
                                    value={userEmail}
                                    onChange={(e) => setUserEmail(e.target.value)}
                                    required
                                />
                            </div>
                            <div className="mb-3">
                                <input
                                    type="password"
                                    className="form-control"
                                    placeholder="Digite uma Senha"
                                    value={userPassword}
                                    onChange={(e) => setUserPassword(e.target.value)}
                                    required
                                />
                            </div>
                            <br />
                            <div className="d-grid gap-2">
                                <button type="submit" className="btn btn-outline-light">Cadastrar</button>
                                <hr />
                                <a href="/" className="btn btn-outline-light">Já sou cadastrado</a>
                            </div>
                        </form>
                    </C.SideLeft>
                </div>
                <div className="col-md-6 px-0">
                    <C.SideRight>
                        <C.StyledNavbar expand="lg">
                            <Navbar.Brand>Expense Control App</Navbar.Brand>
                            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        </C.StyledNavbar>
                        <Carousel>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src="/assets/img/carousel/5.jpg"
                                    alt="Primeiro slide"
                                    style={{ height: '100vh', objectFit: 'cover' }}
                                />
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src="/assets/img/carousel/2.jpg"
                                    alt="Segundo slide"
                                    style={{ height: '100vh', objectFit: 'cover' }}
                                />
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src="/assets/img/carousel/4.jpg"
                                    alt="Terceiro slide"
                                    style={{ height: '100vh', objectFit: 'cover' }}
                                />
                            </Carousel.Item>
                        </Carousel>
                        <C.Footer>
                            Todos os direitos reservados
                        </C.Footer>
                    </C.SideRight>
                </div>
            </div>
        </div>
    );
}

export default CadLogin;
