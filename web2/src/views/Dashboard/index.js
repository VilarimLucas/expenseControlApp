import React, { useState } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import * as C from './style';

import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import Home from './Home';


const index = () => {
  return (
    <Container>
      <Row>
        <Col xs={12} md={3}>
          <C.SideLeft>
            <nav class="nav flex-column nav-side-left">

              <div class="user_profle_side">
                <div class="user_img"><img class="img-responsive" src="/assets/img/users/1.png" alt="#" /></div>
                <div class="user_info">
                  <h6>Nome do Usuário</h6>
                  <p><span class="online_animation"></span> email@ex.com</p>
                </div>
              </div>
              <hr />
              <a class="nav-link active" aria-current="page" href="#">Página Inicial</a>
              <a class="nav-link" href="#">Despesas</a>
              <a class="nav-link" href="#">Renda</a>
              <a class="nav-link" href="#">Educação Continuada</a>
              <a class="nav-link" href="#">Renda Extra</a>
              <a class="nav-link" href="#">Investimentos</a>
              <a class="nav-link" href="#">Configurações</a>
            </nav>
          </C.SideLeft>
        </Col>
        <Col xs={12} md={9}>
          <C.SideRight>
            <Header />
            <Home />
            <Footer />
          </C.SideRight>
        </Col>
      </Row>
    </Container>
  )
}

export default index
