
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';

import ExtraIncome from '../../components/ExtraIncome/ExtraIncome';
import ContinuosEducation from '../../components/ContinuosEducation/ContinuosEducation';
import Investiment from '../../components/Investiment/Investiment';
import Expense from '../../components/Expense/Expense';
import Income from '../../components/Income/Income';

function Home() {
    return (
        <Container>
            <Row>
                <Col xs={12} sm={6} md={6}> <Expense /></Col>
                <Col xs={12} sm={6} md={6}><Income /></Col>
                <Col xs={12} sm={6} md={6}><ExtraIncome /></Col>
                <Col xs={12} sm={6} md={6}><ContinuosEducation /></Col>
            </Row>



            <Row>
                <Col>
                    <Investiment />
                </Col>
            </Row>
        </Container>
    );
}

export default Home;