import styled from "styled-components";


export const SideLeft = styled.div`

    padding: 20px;
    margin-top:2%;
    border-radius: 10px;
    border: 1px solid #3498DB;
    background-color: #fff !important;
    min-height:100%;
    h1, hr{
        color: #000;
    }
    .btn{
        padding: 15px;
        border-radius: 20px;
    }
    .user_profle_side {
    display: flex;
    }
    .user_img img {
    border-radius: 100% 100%;
    }
    .img-responsive {
        max-width: 50%;
    }
    .user_info {
        margin: 0 0 0 15px;
        padding-top: 15px;
    }
    .online_animation {
        width: 12px;
        height: 12px;
        background: #1ed085;
        border-radius: 100%;
        margin-top: 0;
        position: relative;
        top: 1px;
        }
    
`;

export const SideRight = styled.div`

`;
