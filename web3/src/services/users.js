import { http } from './config'

export default {

    list: () => {
        return http.get('user/all')
    },
    save: (user) => {
        return http.post('user/add', user)
    },
    update: (user) => {
        return http.put(`user/${user.id}`, user)
    },
    delete: (user) => {
        return http.delete(`user/${user.id}`)
    }
}